from driver import Platform, Protocol, Driver
import json



class Test(object):

    def __init__(self, outFile, network='none'):
        self.outFile = outFile
        self.network = network
        self.results = []

    def run(self):
        self.simpleDesktopHTTPSRequests(10, 'https://djangocms.thierryve.nl/en/', 'https://djangocms.thierryve.nl')
        self.simpleDesktopQUICRequests(10, 'https://djangocms.thierryve.nl/en/', 'https://djangocms.thierryve.nl')
        self.simpleMobileHTTPSRequests(10, 'https://djangocms.thierryve.nl/en/', 'https://djangocms.thierryve.nl')
        self.simpleMobileQUICRequests(10, 'https://djangocms.thierryve.nl/en/', 'https://djangocms.thierryve.nl')

        # self.simpleDesktopHTTPSRequests(10, 'https://akaunting.thierryve.nl/index.php/auth/login',
        #                                 'https://akaunting.thierryve.nl')
        # self.simpleDesktopQUICRequests(10, 'https://akaunting.thierryve.nl/index.php/auth/login',
        #                                'https://akaunting.thierryve.nl')
        # self.simpleMobileHTTPSRequests(10, 'https://akaunting.thierryve.nl/index.php/auth/login',
        #                                'https://akaunting.thierryve.nl')
        # self.simpleMobileQUICRequests(10, 'https://akaunting.thierryve.nl/index.php/auth/login',
        #                               'https://akaunting.thierryve.nl')

        #self.simpleDesktopHTTPSRequests(5, 'https://crm.thierryve.nl/index.php?action=Login&module=Users', 'https://crm.thierryve.nl')
        #self.simpleDesktopQUICRequests(5, 'https://crm.thierryve.nl/index.php?action=Login&module=Users', 'https://crm.thierryve.nl')
        #self.simpleDesktopHTTPSRequests(10, 'https://www.google.nl')
        #self.simpleDesktopQUICRequests(10, 'https://www.google.nl')
        #self.simpleMobileHTTPSRequests(10, 'https://crm.thierryve.nl')
        #self.simpleMobileQUICRequests(10, 'https://crm.thierryve.nl')

        #self.simpleDesktopHTTPSRequests(10, 'https://chat.thierryve.nl')
        #self.simpleDesktopQUICRequests(10, 'https://chat.thierryve.nl')
        #self.simpleMobileHTTPSRequests(10, 'https://chat.thierryve.nl')
        #self.simpleMobileQUICRequests(10, 'https://chat.thierryve.nl')

        #elf.simpleDesktopHTTPSRequests(10, 'https://video1.thierryve.nl')
        #self.simpleDesktopQUICRequests(10, 'https://video1.thierryve.nl')
        #elf.simpleMobileHTTPSRequests(10, 'https://video1.thierryve.nl')
        #self.simpleMobileQUICRequests(10, 'https://video1.thierryve.nl')

        #self.simpleDesktopHTTPSRequests(5, 'https://master-thesis-quic.appspot.com', 'https://master-thesis-quic.appspot.com')
        #self.simpleDesktopQUICRequests(5, 'https://master-thesis-quic.appspot.com', 'https://master-thesis-quic.appspot.com')
        #self.simpleMobileHTTPSRequests(10, 'https://master-thesis-quic.appspot.com')
        #self.simpleMobileQUICRequests(10, 'https://master-thesis-quic.appspot.com')

        #self.simpleDesktopHTTPSRequests(10, 'https://static.thierryve.nl/10kb.html', 'https://static.thierryve.nl')
        #self.simpleDesktopQUICRequests(10, 'https://static.thierryve.nl/10kb.html', 'https://static.thierryve.nl')
        #self.simpleMobileHTTPSRequests(10, 'https://static.thierryve.nl/10kb.html', 'https://static.thierryve.nl')
        #self.simpleMobileQUICRequests(10, 'https://static.thierryve.nl/10kb.html', 'https://static.thierryve.nl')

        #self.simpleDesktopHTTPSRequests(5, 'https://static.thierryve.nl/100kb.html', 'https://static.thierryve.nl')
        #self.simpleDesktopQUICRequests(5, 'https://static.thierryve.nl/100kb.html', 'https://static.thierryve.nl')
        #self.simpleMobileHTTPSRequests(5, 'https://static.thierryve.nl/100kb.html', 'https://static.thierryve.nl')
        #self.simpleMobileQUICRequests(5, 'https://static.thierryve.nl/100kb.html', 'https://static.thierryve.nl')
        #
        # self.simpleDesktopHTTPSRequests(5, 'https://static.thierryve.nl/1mb.html', 'https://static.thierryve.nl')
        # self.simpleDesktopQUICRequests(5, 'https://static.thierryve.nl/1mb.html', 'https://static.thierryve.nl')
        # self.simpleMobileHTTPSRequests(5, 'https://static.thierryve.nl/1mb.html', 'https://static.thierryve.nl')
        # self.simpleMobileQUICRequests(5, 'https://static.thierryve.nl/1mb.html', 'https://static.thierryve.nl')
        #
        #self.simpleDesktopHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/10kb.html', 'https://master-thesis-quic.appspot.com')
        #self.simpleDesktopQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/10kb.html', 'https://master-thesis-quic.appspot.com')
        #self.simpleMobileHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/10kb.html', 'https://master-thesis-quic.appspot.com')
        #self.simpleMobileQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/10kb.html', 'https://master-thesis-quic.appspot.com')

        # self.simpleDesktopHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/100kb.html',
        #                                 'https://master-thesis-quic.appspot.com')
        # self.simpleDesktopQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/100kb.html',
        #                                'https://master-thesis-quic.appspot.com')
        # self.simpleMobileHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/100kb.html',
        #                                'https://master-thesis-quic.appspot.com')
        # self.simpleMobileQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/100kb.html',
        #                               'https://master-thesis-quic.appspot.com')
        #
        # self.simpleDesktopHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/1mb.html', 'https://master-thesis-quic.appspot.com')
        # self.simpleDesktopQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/1mb.html', 'https://master-thesis-quic.appspot.com')
        # self.simpleMobileHTTPSRequests(5, 'https://master-thesis-quic.appspot.com/static/1mb.html', 'https://master-thesis-quic.appspot.com')
        # self.simpleMobileQUICRequests(5, 'https://master-thesis-quic.appspot.com/static/1mb.html', 'https://master-thesis-quic.appspot.com')

        self.save()

    def simpleDesktopHTTPSRequests(self, count, url, domain):
        for x in range(count):
            driver = Driver(Platform.DESKTOP, Protocol.HTTPS, domain) \
                .defaultOptions() \
                .start() \
                .get(url)
            duration = driver.duration()
            self.results.append({'testtype': 'simple',
                                 'platform': Platform.DESKTOP.name,
                                 'protocol': Protocol.HTTPS.name,
                                 'url': url,
                                 'network': self.network,
                                 'duration': duration
                                 })

            print('{} - request {} for platform {} on protocol {} in {}ms'.format(x, url, Platform.DESKTOP.name,
                                                                          Protocol.HTTPS.name, duration))

            driver.stop(delay=0)

    def simpleMobileHTTPSRequests(self, count, url, domain):
        for x in range(count):
            driver = Driver(Platform.MOBILE, Protocol.HTTPS, domain) \
                .defaultOptions() \
                .start() \
                .get(url)
            duration = driver.duration()
            self.results.append({'testtype': 'simple',
                                 'platform': Platform.MOBILE.name,
                                 'protocol': Protocol.HTTPS.name,
                                 'url': url,
                                 'network': self.network,
                                 'duration': duration
                                 })

            print('{} - request {} for platform {} on protocol {} in {}ms'.format(x, url, Platform.MOBILE.name,
                                                                          Protocol.HTTPS.name, duration))

            driver.stop(delay=0)

    def simpleDesktopQUICRequests(self, count, url, domain):
        for x in range(count):
            driver = Driver(Platform.DESKTOP, Protocol.QUIC, domain) \
                .defaultOptions() \
                .start() \
                .get(url)
            duration = driver.duration()
            self.results.append({'testtype': 'simple',
                                 'platform': Platform.DESKTOP.name,
                                 'protocol': Protocol.QUIC.name,
                                 'url': url,
                                 'network': self.network,
                                 'duration': duration
                                 })

            print('{} - request {} for platform {} on protocol {} in {}ms'.format(x, url, Platform.DESKTOP.name,
                                                                          Protocol.QUIC.name, duration))

            driver.stop(delay=0)

    def simpleMobileQUICRequests(self, count, url, domain):
        for x in range(count):
            driver = Driver(Platform.MOBILE, Protocol.QUIC, domain) \
                .defaultOptions() \
                .start() \
                .get(url)
            duration = driver.duration()
            self.results.append({'testtype': 'simple',
                                 'platform': Platform.MOBILE.name,
                                 'protocol': Protocol.QUIC.name,
                                 'url': url,
                                 'network': self.network,
                                 'duration': duration
                                 })

            print('{} - request {} for platform {} on protocol {} in {}ms'.format(x, url, Platform.MOBILE.name,
                                                                          Protocol.QUIC.name, duration))

            driver.stop(delay=0)

    def save(self):
        with open(self.outFile, 'w') as f:
            json.dump(self.results, f)