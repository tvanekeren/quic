import json
from pprint import pprint

import plotly
import plotly.graph_objs as go

# Create random data with numpy
import numpy as np


data = json.load(open('results/006/latency200_006.json'))
boxData = {}
for x in data:
    t = ''
    #if x['url'] == 'https://video1.thierryve.nl':
    #    t = 'video'
    if x['url'] == 'https://crm.thierryve.nl':
        t = 'crm'
    else:
        continue
    #elif x['url'] == 'https://chat.thierryve.nl':
    #    t = 'chat'
    #elif x['url'] == 'https://master-thesis-quic.appspot.com':
    #    t = 'guestbook'

    if x['platform'] != 'MOBILE':
        continue

    key = '{}|{}|{}'.format(t, x['protocol'], x['platform'])
    if not key in boxData:
        boxData.update({key: []})

    boxData.get(key).append(x['duration'])

pprint(boxData)

graphData = []
for key, value in boxData.items():
    # remove first and last entry
    value.sort()
    value = value[:-1]
    value.pop(0)

    graphData.append(
        go.Box(
            y=value,
            boxpoints='all',
            jitter=0.3,
            pointpos=-1.8,
            name=key
        )
    )

plotly.offline.plot(graphData, image='png')

'''
N = 1000
random_x = np.random.randn(N)
random_y = np.random.randn(N)

# Create a trace
trace = go.Scatter(
    x = random_x,
    y = random_y,
    mode = 'markers'
)

data = [trace]

# Plot and embed in ipython notebook!
plotly.offline.plot(data, filename='basic-scatter.html')

# or plot with: plot_url = py.plot(data, filename='basic-line')



class Result(dict):
    testtype = ""
    url = ""
    network = ""
    duration = -1

    def __init__(self, testtype, url, network, duration):
        self.testtype = testtype
        self.url = url
        self.network = network
        self.duration = duration

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

'''

