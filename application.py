from test import Test
from network import Network

Network().reset()

Test('log/defaultnetwork_djangocms.json', 'none').run()
#
Network().reset()
Network().delay(200)
Test('log/latency200_djangocms.json', 'latency_200').run()
#
#Network().reset()
#Network().jitter(200, 50)
#Test('log/jitter20050_005.json', 'jitter_20050').run()
#
Network().reset()
Network().packetloss(1)
Test('log/packetloss1_djangocms.json', 'packetloss_1').run()
#
Network().reset()
Network().packetlossburst(1, 25)
Test('log/packetlossburst125_djangocmsjson', 'packetlossburst_125').run()
#
Network().reset()
Network().duplication(10)
Test('log/duplication10_djangocms.json', 'duplication_10').run()
#
Network().reset()
Network().corruption(1)
Test('log/corruption1_akaunting.json', 'corruption_1').run()
#
Network().reset()
Network().reordering(10, 25, 50)
Test('log/reordering2550_djangocm.json', 'reordering2550').run()

#Network().international_internet_path()
#Test('log/international_internet_path_005.json', 'international_internet_path').run()

Network().reset()




'''
driver = Driver(Platform.MOBILE, Protocol.QUIC)\
    .defaultOptions()\
    .start()

driver.get('https://crm.thierryve.nl')


driver.stop(delay=5)
'''

'''
    .delay()\
    .clear()\
    .get('https://chat.thierryve.nl')\
    .duration()\
    .dumplog('chat.json')\
    .delay()\
    .clear()\
    .get('https://master-thesis-quic.appspot.com')\
    .duration()\
    .dumplog('gae.json')\
    .stop(delay=5)
'''

'''

Driver('desktop', 'quic')\
    .defaultOptions()\
    .headless()\
    .start()\
    .get('https://chat.thierryve.nl')\
    .dumplog('devtools2.json')\
    .stop(delay=0)  

Driver('desktop', 'https')\
    .defaultOptions()\
    .headless()\
    .start()\
    .get('https://crm.thierryve.nl')\
    .dumplog('devtools3.json')\
    .stop(delay=0)

'''